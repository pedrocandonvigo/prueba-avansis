
class NewsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	@IBOutlet weak var tableView: UITableView!	
	
	let searchBarController = UISearchController()
	var presenter: NewsPresenter?
	var news = [NewsModel]()
	var foundNews = [NewsModel]()
	
	func viewDidLoad() {
		super.viewDidLoad()
		setupView()
		loadData()
	}
	
	func setupView() {
		presenter = NewsPresenter()
		presenter?.controller = self
		tableView.delegate = self
		tableView.datasource = self
		searchBarController.searchResulUpdater = self
	}
	
	func loadData() {
		presenter?.getNews()
	}
	
	func loadTable(_ news: [NewsModel]) {
		self.news = news
		self.foundNews = news
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int { 
		return foundNews.count
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		//Load news data (title, description, date, image) for each custom row NewTableViewCell: UITableViewCell (not included :( )
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		//Open and load a new view using navigationController like :
		let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewsDetailVC") as? NewsDetailViewController
		vc.newDetail = self.foundNews[indexPath.row]
		self.navigationController?.pushViewController(vc!, animated: true)
			
	} 
	
	func updateSearchResults(for searchBarController: UISearchController) {
		guard let text =  searchBarController.searchBar.text else { return }
		foundNews = [NewsModel]()
		for new in news {
			if new.containsText(text: text) {
				foundNews.append(new)
			}
		}
		
		tableView.reloadData()
	}

}