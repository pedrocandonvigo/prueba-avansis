//Model

class NewsModel: Decodable {
	var title: String
	var desc: String
	var date: Date
	var image: String
	
	enum CodingKeys: String, CodingKey {
		case title
		case description
		case date
		case image
	}
	
	func containsText(text: String) -> Bool {
		return title.lowercased().contains(text.lowercased())
	}

}