//API

struct DefaultsKeys {
    static let oldNews = "oldNews"
}

protocol NewsAPIDelegate: AnyObject {
	func getNewsSuccess(newsArray: [NewsModel])
	func getNewsError(error: String)

}

import Alamofire

class NewsAPI {
	
	weak var delegate: NewsAPIDelegate?
	let defaults = UserDefaults.standard
	
	func requestNews() {
		
		AF.request("https://newsapi.org/s/google-news-api").responseDecodable(of: [NewsModel]) {(response) in
			if let error = response.result.error as? AFError { delegate?getNewsError(error._code.toString()) }
			guard let news = response.value else { delegate?getNewsError("Data error") }
			delegate?.getNewsSuccess(news)
			defaults.set([NewsModel], forKey: DefaultsKeys.oldNews)

		}
	
	}

}

extension NewsAPIDelegate {
	func getNewsSuccess(newsArray: [NewsModel]) {}
	func getNewsError(error: String) {}
	
}


