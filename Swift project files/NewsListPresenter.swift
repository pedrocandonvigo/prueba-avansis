
//Presenter
class NewsPresenter {
	weak var controller: NewsListViewController
	var newsRepository: NewsAPI = NewsAPI()
	
	init() {
		newsRepository.delegate  = self
	
	}
		
	func getNews() {
		newsRepository.getNews()
	}
	
}

extension NewsPresenter: NewsAPIDelegate {
	func getNewsSuccess(newsArray: [NewsModel]) {
		controller.loadTable(newsArray)
	}
	
	func getNewsError(error: String) {
		controller.showError(error)
		let defaults = UserDefaults.standard
		if let oldNews = defaults.object(forKey: DefaultsKeys.oldNews) as? [NewsModel] ?? [NewsModel]() {
			controller.loadTable(oldNews)
		}
	}

}
