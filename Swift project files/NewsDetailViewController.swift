

class NewsDetailViewController: UIViewController {
	@IBOutlet weak var title: UILabel!
	@IBOutlet weak var desc: UILabel!
	@IBOutlet weak var date: UILabel!
	@IBOutlet weak var imageView: UIImageView!
	
	var newDetail: [NewsModel]?
	
	func viewWillAppear() {
		super.viewWillAppear()
		loadData()
	}
	
	
	func loadData() {
		title.text = newDetail.title
		desc.text = newDetail.description
		
		let formatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd-HH:mm"//Apply date format text
		date.text = formatter.string(from: newDetail.date)
		
		if let data = Data(base64Encoded: newDetail.image, options: .ignoreUnknownCharacters){
            imageView.image = UIImage(data: data)
        }
		
	}
	
}